# -*- coding: utf-8 -*-


def burrows_wheeler_matching(text, patterns):

    text_length = len(text)

    matches = []

    last_column = []

    for c in text:

        i = 1

        while 1:

            if (c, i) not in last_column:

                last_column.append((c, i))
                break

            else:

                i += 1

    first_column = list(last_column)

    first_column.sort()

    for pattern in patterns:

        # BWMATCHING(FirstColumn, LastColumn, Pattern, LastToFirst)
        # top ← 0
        # bottom ← |LastColumn| − 1
        # while top ≤ bottom
        #     if Pattern is nonempty
        #         symbol ← last letter in Pattern
        #         remove last letter from Pattern
        #         if positions from top to bottom in LastColumn contain an occurrence of symbol
        #             topIndex ← first position of symbol among positions from top to bottom in LastColumn
        #             bottomIndex ← last position of symbol among positions from top to bottom in LastColumn
        #             top ← LastToFirst(topIndex)
        #             bottom ← LastToFirst(bottomIndex)
        #         else
        #             return 0
        #     else
        #         return bottom − top + 1

        top = 0
        bottom = text_length - 1

        while top <= bottom:

            if len(pattern) > 0:

                symbol = pattern[-1:]

                pattern = pattern[:-1]

                found = False

                top_index = text_length
                bottom_index = 0

                range_top_bottom = range(top, bottom + 1)

                for i in range_top_bottom:

                    if last_column[i][0] == symbol:

                        found = True

                        top_index = min(i, top_index)
                        bottom_index = max(i, bottom_index)

                        top = first_column.index(last_column[top_index])
                        bottom = first_column.index(last_column[bottom_index])

                if not found:

                    matches.append(0)

                    break

            else:

                matches.append(bottom - top + 1)

                break

    return matches


f = open('dataset_300_8.txt', 'r')

example = f.read().splitlines()

text = example[0]
patterns = example[1].split()

# print ' ' . join([str(i) for i in burrows_wheeler_matching('TCCTCTATGAGATCCTATTCTATGAAACCTTCA$GACCAAAATTCTCCGGC', ['CCT', 'CAC', 'GAG', 'CAG', 'ATC'])])

# Output:
# 2 1 1 0 1

# print ' ' . join([str(i) for i in burrows_wheeler_matching(text, patterns)])

# Output
# 0 0 0 0 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 0 0 0 0 1 1 0 1 0 1 1 0 0 0 1 0 0 1 0 0 0 1 0 0 0 0 1 1 1 1 1 0 0 1 0 1 0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0 1 0 1 1 0 0 1 0 0 1 0 0 1 0 0 1 1 0 0 0 0 0 0 0 1 0 0 0 1 0 1 0 1 0 0 0 0 0 1 0 1 0 0 1 0 1 1 1 1 0 1 1 0 0 0 1 0 0 1 0 1 0 0 1 1 1 0 0 0 1 0 0 0 1 1 0 0 1 1 1 0 0 1 0 1 0 0 0 0 1 1 0 1 1 1 0 1 0 0 0 0 1 0 0 0 0 1 0 1 0 1 1 1 0 0 0 1 0 0 1 0 1 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 0 1 1 0 0 0 1 1 0 0 1 0 0 0 1 0 0 1 0 1 0 1 0 0 0 1 1 1 1 0 0 1 1 0 0 0 0 1 1 0 0 0 1 0 1 1 0 0 1 0 1 0 0 0 0 0 1 0 1 1 1 0 0 1 0 0 0 1 0 0 0 1 0 1 0 1 0 0 0 1 0 0 0 0 1 0 1 1 0 0 1 0 1 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 1 1 0 0 0 1 0 0 0 0 1 1 0 0 1 0 1 0 0 1 0 0 0 1 0 0 1 1 1 0 0 1 1 0 0 1 0 1 0 1 0 1 0 0 0 0 0 0 1 0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 0 0 0 1 1 0 1 0 1 0 1 0 0 0 0 0 0 1 0 1 0 1 1 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 1 0 0 0 0 1 1 0 0 1 0 1 0 0 1 1 0 0 1 0 1 1 1 1 0 0 0 0 0 1 0 1 1 0 1 0 0 0 0 1 1 0 1 0 1 1 0 1 0 0 0 1 1 0 0 0 0 1 0 1 0 0 0 1 1 1 0 0 1 0 0 0 0 1 1 0 1 0 0 1 1 1 0 0 1 0 0 0 0 0 1 1 0 0 0 1 1 1 0 0 1 0 1 0 0 0 1 1 0 1 0 1 0 1 1 0 0 0 1 0 0 0 0 1 0 1 0 0 0 1 0 1 0 1 0 0 0 0 1 0 1 1 0 1 1 0 1 0 0 0 0 1 1 0 1 0 0 0 1 0 0 0 0 0 1 1 0 1 1 0 1 0 0 0 1 0 1 1 0 1 0 0 0 0 1 1 0 1 1 0 0 1 1 0 1 0 0 1 1 0 1 0 0 0 1 0 0 1 0 0 0 0 0 0 1 0 1 0 0 0 0 0 1 1 0 1 1 0 0 0 0 1 0 1 0 1 0 1 0 0 1 1 0 0 0 1 0 0 1 1 0 0 0 0 0 1 0 1 0 1 1 0 0 0 0 0 1 0 0 1 0 0 1 0 1 1 1 0 0 1 0 0 1 1 1 1 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0 1 0 1 1 0 0 0 0 0 0 0 1 1 1 1 0 1 1 0 1 0 0 0 0 0 0 0 0 0 1 0 0 1 1 0 1 1 0 0 1 1 0 1 0 0 1 1 0 1 1 0 0 0 0 0 1 1 1 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 1 0 0 1 1 0 0 0 0 0 0 0 1 1 0 1 1 1 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 0 1 1 0 0 0 0 1 0 1 0 0 1 0 0 0 0 0 0 1 0 1 0 1 1 0 0 1 0 0 1 0 1 0 1 0 0 1 0 1 0 0 0 0 0 1 1 1 0 1 0 0 0 0 0 1 1 0 1 1 0 0 0 1 1 0 0 0 0 0 0 0 0 1 0 1 0 0 0 0 1 0 1 0 0 1 1 0 1 1 0 1 0 0 1 0 0 1 0 0 1 0 1 0 1 0 0 0 1 0 1 0 1 1 0 1 0 0 0 0 1 0 0 1 0 1 0 0 0 0 0 0 0 0 1 0 0 1 1 1 1 0 1 1 0 0 0 1 1 0 0 0 0 1 0 1 1 1 1 1 1 0 0 1 1 1 1 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 1 0 0 1 0 1 0 0 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1 0 1 0 1 0 1 0 1 0 1 1 1 0 0 1 1 0 1 0 0 1 0 0 1 1 0 1 1 0 1 1 1 0 1 0 1 0 0 1 0 1 0 0 0 0 0 0 0 1 0 0 1 1 0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0 1 0 1 0 1 0 1 1 0 1 0 0 1 1 0 1 1 0 1 0 1 1 0 1 1 1 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 1 0 1 1 0 0 0 1 1 0 0 0 0 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0 0 1 1 0 1 1 0 0 1 1 0 1 0 0 0 0 1 0 0 1 0 0 0 1 0 1 1 0 0 1 0 1 1 1 1 0 1 1 0 0 1 1 1 1 1 0 1 0 0 0 1 0 0 1 1 0 1 0 0 1 0 0 0 0 0 1 0 1 1 0 0 1 0 0 0 1 1 1 0 0 1 1 0 1 0 1 0 0 1 1 1 0 0 0 0 0 1 0 1 0 0 0 1 0 0 0 1 1 0 0 0 1 1 0 0 1 1 0 0 0 1 0 0 1 0 1 1 1 0 1 1 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0 0 1 1 1 1 0 0 1 1 1 1 0 0 1 1 1 0 0 1 0 1 0 0 0 1 0 0 0 0 0 1 1 1 0 0 1 0 1 1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0 0 1 1 1 1 0 1 0 1 0 0 1 0 0 0 0 1 0 1 1 1 1 0 0 0 1 0 1 1 1 0 0 0 1 0 0 0 0 1 0 1 1 0 0 0 1 0 0 0 0 1 0 0 0 1 1 1 0 0 1 0 1 0 0 0 1 0 1 0 0 0 0 1 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 0 1 0 0 0 1 1 0 1 0 1 0 0 0 0 1 0 0 0 0 0 0 1 1 1 0 1 1 1 0 1 0 0 1 0 0 0 1 1 0 0 1 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 1 1 0 0 0 0 1 1 1 1 0 1 0 1 0 1 1 0 1 0 1 1 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 1 0 1 1 0 0 1 1 0 0 0 0 1 0 0 0 1 1 0 1 1

print ' ' . join([str(i) for i in burrows_wheeler_matching(text, patterns)])

f.close()
