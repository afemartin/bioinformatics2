# -*- coding: utf-8 -*-


def limb_length(n, j, matrix):

    length = 999999

    for i in range(0, n):

        for k in range(i, n):

            if i != j and k != j:

                result = (matrix[i][j] + matrix[j][k] - matrix[i][k]) / 2

                length = min(length, result)

    return length


f = open('dataset_10329_11.txt', 'r')

example = f.read().splitlines()

n = int(example[0])
j = int(example[1])
matrix = example[2:]

for i in range(0, len(matrix)):

    matrix[i] = [int(d) for d in matrix[i].split()]

# print limb_length(n, j, matrix)

# Output
# 2

# print limb_length(n, j, matrix)

# Output
# 534

print limb_length(n, j, matrix)

f.close()