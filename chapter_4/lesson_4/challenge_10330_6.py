# -*- coding: utf-8 -*-


def print_matrix(matrix):

    print '**************'

    for row in matrix:

        print "\t" . join([str(i) for i in row])

    print '**************'


def calculate_limb_length(n, j, matrix):

    length = 999999

    for i in range(0, n):

        for k in range(i, n):

            if i != j and k != j:

                result = (matrix[i][j] + matrix[j][k] - matrix[i][k]) / 2

                length = min(length, result)

    return length


def additive_phylogeny(n, matrix):

    # AdditivePhylogeny(D, n)
    # if n = 2
    #     return the tree consisting of a single edge of length D1,2
    # limbLength ← Limb(D, n)
    #
    # for j ← 1 to n - 1
    #     Dj,n ← Dj,n - limbLength
    #     Dn,j ← Dj,n
    #
    # remove n-th row and column from D
    #
    # T ← AdditivePhylogeny(D, n - 1)
    #
    # (i,n,k) ← three leaves such that Di,k = Di,n + Dn,k
    # x ← Di,n
    #
    # v ← the (potentially new) node in T located at distance x from leaf i on the path between i and k
    #
    # add leaf n back to T by creating a limb (v, n) of length limbLength
    # return T

    tree = []

    if n == 2:

        return {'0': {'1': matrix[0][1]}}

    limb_length = calculate_limb_length(n, n-1, matrix)

    print limb_length

    for j in range(0, n):

        if j != n-1:

            matrix[j][n-1] = matrix[j][n-1] - limb_length
            matrix[n-1][j] = matrix[j][n-1]

    print_matrix(matrix)

    matrix = [[elem for elem in row[:-1]] for row in matrix[:-1]]

    tree = additive_phylogeny(n-1, matrix)

    for i in range(0, n-1):

        for k in range(0, n-1):

            if i < k and matrix[i][k] == matrix[i][n-2] + matrix[k][n-2]:

                print matrix[i][k]

                print str(i) + ' - ' + str(k)

                x = matrix[i][n-2]

    print '*****'

    print x

    print '*****'

    print tree

    tree[str(n-1)] = 0

    # T ← AdditivePhylogeny(D, n - 1)
    # v ← the (potentially new) node in T located at distance x from leaf i on the path between i and k
    #
    # add leaf n back to T by creating a limb (v, n) of length limbLength
    # return T

    return tree


f = open('dataset_10330_6_example.txt', 'r')

example = f.read().splitlines()

n = int(example[0])
matrix = example[1:]

for i in range(0, len(matrix)):

    matrix[i] = [int(d) for d in matrix[i].split()]

print additive_phylogeny(n, matrix)

# Output
# 2

# print additive_phylogeny(n, matrix)

# Output
# 534

# print additive_phylogeny(n, matrix)

f.close()