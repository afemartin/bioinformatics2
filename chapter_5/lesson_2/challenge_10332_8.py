# -*- coding: utf-8 -*-


def upgma(n, matrix):

    # UPGMA(D, n)
    #  form n clusters, each containing a single element i (for i from 1 to n)
    #  construct a graph T by assigning a node to each cluster (without adding any edges)
    #  for every node v in T
    #   Age(v) ← 0
    #  while there is more than one cluster
    #   find two closest clusters C1 and C2 (break ties arbitrarily)
    #   merge C1 and C2 into a new cluster C
    #   add a new node C to T and connect it to nodes C1 and C2 by directed edges
    #   Age(C) ← DC1,C2 / 2
    #   remove rows and columns of D corresponding to C1 and C2
    #   add a row and column to D for C by recomputing DC,C* for each C* ≠ C
    #  root ← the node in T corresponding to the cluster C
    #  for each edge (v,w) in T
    #   Length(v,w) ← Age(v) - Age(w)
    #  return T

    clusters = [str(cluster) for cluster in range(0, n)]

    edges = {}

    nodes_weight = {}

    mapping = {}

    for cluster in clusters:

        edges[cluster] = {}

        nodes_weight[cluster] = 0

        mapping[cluster] = cluster

    matrix_trimmed = {}

    for i1, row in enumerate(matrix):

        matrix_trimmed[str(i1)] = {}

        for i2, node in enumerate(row):

            matrix_trimmed[str(i1)][str(i2)] = node

    while len(clusters) > 1:

        print '********************************************'

        # print clusters
        # print edges
        # print matrix_trimmed

        # find two closest clusters C1 and C2 (break ties arbitrarily)

        closest_clusters = []
        closest_clusters_distance = 999999

        for i in clusters:

            for j in clusters:

                if i != j and matrix_trimmed[i][j] < closest_clusters_distance:

                    closest_clusters = [i, j]
                    closest_clusters_distance = matrix_trimmed[i][j]

        # print closest_clusters
        # print closest_clusters_distance

        # merge C1 and C2 into a new cluster C

        c1 = closest_clusters[0]
        c2 = closest_clusters[1]

        c = c1 + '+' + c2

        # print 'c: ' + c
        # print 'c1: ' + c1
        # print 'c2: ' + c2

        new_node_index = str(len(edges))

        mapping[c] = new_node_index

        edges[c] = {}

        nodes_weight[c] = closest_clusters_distance

        edges[c1][c] = (closest_clusters_distance - nodes_weight[c1]) / 2.000
        edges[c2][c] = (closest_clusters_distance - nodes_weight[c2]) / 2.000

        clusters.remove(c1)
        clusters.remove(c2)

        matrix_trimmed.pop(c1)
        matrix_trimmed.pop(c2)

        clusters.append(c)

        matrix_trimmed[c] = {c: 0}

        # print matrix_trimmed

        for row in matrix_trimmed:

            # print 'row: ' + row

            if row != c:

                matrix_trimmed[row].pop(c1, None)
                matrix_trimmed[row].pop(c2, None)

                average = 0.000

                nodes_c = c.split('+')
                nodes_row = row.split('+')

                for node_c in nodes_c:

                    for node_row in nodes_row:

                        average += matrix[int(node_c)][int(node_row)]

                average /= (len(nodes_c) + len(nodes_row))

                if len(nodes_c) + len(nodes_row) > 3:

                    print 'FUCK: ' + 'row: ' + mapping[row]

                matrix_trimmed[row][c] = average

        for row in matrix_trimmed:

            if row != c:

                matrix_trimmed[c][row] = matrix_trimmed[row][c]

    edges_processed = []

    for edge_start in edges:

        for edge_end in edges[edge_start]:

            # print edge_start
            # print edge_end

            ascending = "%d->%d:%.3f" % (int(mapping[edge_end]), int(mapping[edge_start]), round(edges[edge_start][edge_end], 3))
            descending = "%d->%d:%.3f" % (int(mapping[edge_start]), int(mapping[edge_end]), round(edges[edge_start][edge_end], 3))

            edges_processed.append(ascending)
            edges_processed.append(descending)

    edges_processed.sort()

    return edges_processed


f = open('dataset_10332_8.txt', 'r')

example = f.read().splitlines()

n = int(example[0])
matrix = example[1:]

for i in range(0, len(matrix)):

    matrix[i] = [int(d) for d in matrix[i].split()]

# print "\n" . join(upgma(n, matrix))

# Output
# 0->5:7.000
# 1->6:8.833
# 2->4:5.000
# 3->4:5.000
# 4->2:5.000
# 4->3:5.000
# 4->5:2.000
# 5->0:7.000
# 5->4:2.000
# 5->6:1.833
# 6->5:1.833
# 6->1:8.833

# print "\n" . join(upgma(n, matrix))

# Output
# 0->28:25.500
# 1->36:59.500
# 2->36:59.500
# 3->37:67.750
# 4->27:25.000
# 5->29:27.000
# 6->33:45.000
# 7->27:25.000
# 8->30:28.000
# 9->39:117.250
# 10->44:168.000
# 11->35:45.750
# 12->32:30.000
# 13->31:28.500
# 14->41:131.667
# 15->33:45.000
# 16->44:168.000
# 17->28:25.500
# 18->38:68.500
# 19->34:45.500
# 20->32:30.000
# 21->34:45.500
# 22->38:68.500
# 23->30:28.000
# 24->29:27.000
# 25->31:28.500
# 26->48:235.375
# 27->4:25.000
# 27->35:20.750
# 27->7:25.000
# 28->17:25.500
# 28->40:101.125
# 28->0:25.500
# 29->24:27.000
# 29->5:27.000
# 29->37:40.750
# 30->8:28.000
# 30->42:128.125
# 30->23:28.000
# 31->25:28.500
# 31->13:28.500
# 31->45:201.125
# 32->12:30.000
# 32->43:134.375
# 32->20:30.000
# 33->39:72.250
# 33->6:45.000
# 33->15:45.000
# 34->42:110.625
# 34->19:45.500
# 34->21:45.500
# 35->41:85.917
# 35->27:20.750
# 35->11:45.750
# 36->1:59.500
# 36->43:104.875
# 36->2:59.500
# 37->3:67.750
# 37->49:189.155
# 37->29:40.750
# 38->40:58.125
# 38->18:68.500
# 38->22:68.500
# 39->9:117.250
# 39->33:72.250
# 39->46:113.417
# 40->38:58.125
# 40->28:101.125
# 40->45:103.000
# 41->35:85.917
# 41->14:131.667
# 41->50:127.068
# 42->48:79.250
# 42->30:128.125
# 42->34:110.625
# 43->32:134.375
# 43->46:66.292
# 43->36:104.875
# 44->47:67.292
# 44->16:168.000
# 44->10:168.000
# 45->31:201.125
# 45->47:5.667
# 45->40:103.000
# 46->39:113.417
# 46->43:66.292
# 46->49:26.238
# 47->44:67.292
# 47->50:23.443
# 47->45:5.667
# 48->26:235.375
# 48->52:59.289
# 48->42:79.250
# 49->51:29.874
# 49->46:26.238
# 49->37:189.155
# 50->41:127.068
# 50->47:23.443
# 50->51:28.045
# 51->52:7.884
# 51->50:28.045
# 51->49:29.874
# 52->48:59.289
# 52->51:7.884

print "\n" . join(upgma(n, matrix))

f.close()