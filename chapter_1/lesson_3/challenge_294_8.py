# -*- coding: utf-8 -*-


def graph_has_path(graph, node, c):

    ending_node = 0

    for edge in graph:

        if edge[0] == node and edge[2] == c:

            ending_node = edge[1]

    return ending_node


def trie_construction(patterns):

    # TRIECONSTRUCTION(Patterns)
    #     Trie ← a graph consisting of a single node root
    #     for each string Pattern in Patterns
    #         currentNode ← root
    #         for i ← 1 to |Pattern|
    #             if there is an outgoing edge from currentNode with label currentSymbol
    #                 currentNode ← ending node of this edge
    #             else
    #                 add a new node newNode to Trie
    #                 add a new edge from currentNode to newNode with label currentSymbol
    #                 currentNode ← newNode
    #     return Trie

    trie = []

    last_node = 0

    for pattern in patterns:

        current_node = 0

        for c in pattern:

            ending_node = graph_has_path(trie, current_node, c)

            if ending_node:

                current_node = ending_node

            else:

                next_node = max(last_node + 1, current_node + 1)

                trie.append([current_node, next_node, c])

                # print str(current_node) + '->' + str(next_node) + ':' + c

                current_node = next_node

                last_node = next_node

    return trie


def is_leaf_node(graph, node):

    for edge in graph:

        if edge[0] == node:

            return False

    return True


def prefix_trie_matching(text, trie):

    # PREFIXTRIEMATCHING(Text, Trie)
    #     symbol ← first letter of Text
    #     v ← root of Trie
    #     while forever
    #         if v is a leaf in Trie
    #             return the pattern spelled by the path from the root to v
    #         else if there is an edge (v, w) in Trie labeled by symbol
    #             symbol ← next letter of Text
    #             v ← w
    #         else
    #             output "no matches found"
    #             return

    i = 0

    node = 0

    while 1:

        leaf = is_leaf_node(trie, node)

        if i >= len(text):

            return leaf

        symbol = text[i]

        node = graph_has_path(trie, node, symbol)

        if leaf:

            # print text[:i]
            return True

        elif node > 0:

            i += 1

        else:

            return False


def trie_matching(text, trie):

    # TRIEMATCHING(Text, Trie)
    #     while Text is nonempty
    #         PREFIXTRIEMATCHING(Text, Trie)
    #         remove first symbol from Text

    position = 0

    positions = []

    while len(text) > 0:

        if prefix_trie_matching(text, trie):

            positions.append(position)

        text = text[1:]

        position += 1

    return positions


def multiple_pattern_matching(text, patterns):

    trie = trie_construction(patterns)

    positions = trie_matching(text, trie)

    return positions


f = open('dataset_294_8.txt', 'r')

example = f.read().splitlines()

text = example[0]
patterns = example[1:]

# print ' ' . join([str(s) for s in multiple_pattern_matching('AATCGGGTTCAATCGGGGT', ['ATCG', 'GGGT'])])

# Output:
# 1 4 11 15

# print ' ' . join([str(s) for s in multiple_pattern_matching(text, patterns)])

# Output:
# 108 115 312 319 375 382 500 565 1178 1185 1265 1858 2113 3107 3229 3236 3525 3537 3668 3675 3722 3729 4144 4151 4291 4315 4477 5345 5352 5664 6449 6456 6652 6659 6721 7105 7424 7431 7800 7868 7875 8078 8304 8311 8523 8536

print ' ' . join([str(s) for s in multiple_pattern_matching(text, patterns)])

f.close()
