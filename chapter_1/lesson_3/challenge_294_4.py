# -*- coding: utf-8 -*-


def graph_has_path(graph, node, c):

    ending_node = 0

    for edge in graph:

        if edge[0] == node and edge[2] == c:

            ending_node = edge[1]

    return ending_node


def trie_construction(patterns):

    # TRIECONSTRUCTION(Patterns)
    #     Trie ← a graph consisting of a single node root
    #     for each string Pattern in Patterns
    #         currentNode ← root
    #         for i ← 1 to |Pattern|
    #             if there is an outgoing edge from currentNode with label currentSymbol
    #                 currentNode ← ending node of this edge
    #             else
    #                 add a new node newNode to Trie
    #                 add a new edge from currentNode to newNode with label currentSymbol
    #                 currentNode ← newNode
    #     return Trie

    trie = []

    last_node = 0

    for pattern in patterns:

        current_node = 0

        for c in pattern:

            ending_node = graph_has_path(trie, current_node, c)

            if ending_node:

                current_node = ending_node

            else:

                next_node = max(last_node + 1, current_node + 1)

                trie.append([current_node, next_node, c])

                print str(current_node) + '->' + str(next_node) + ':' + c

                current_node = next_node

                last_node = next_node


f = open('dataset_294_4.txt', 'r')

example = f.read().splitlines()

# trie_construction(['ATAGA', 'ATC', 'GAT'])

# 0->1:A
# 1->2:T
# 2->3:A
# 3->4:G
# 4->5:A
# 2->6:C
# 0->7:G
# 7->8:A
# 8->9:T

trie_construction(example)

f.close()
