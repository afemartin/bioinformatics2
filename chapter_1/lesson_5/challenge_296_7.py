# -*- coding: utf-8 -*-


def find_shortest_non_shared(text1, text2):

    pattern = ''

    text1_length = len(text1)
    text2_length = len(text2)

    min_text_length = min(text1_length, text2_length)

    pattern_length = 1

    while pattern_length < min_text_length:

        for start in range(0, text1_length - pattern_length + 1):

            pattern = text1[start:start+pattern_length]

            if text2.find(pattern) < 0:

                return pattern

        for start in range(0, text2_length - pattern_length + 1):

            pattern = text2[start:start+pattern_length]

            if text1.find(pattern) < 0:

                return pattern

        pattern_length += 1

    return pattern


f = open('dataset_296_7.txt', 'r')

example = f.read().splitlines()

example1 = example[0]
example2 = example[1]

# print find_shortest_non_shared('CCAAGCTGCTAGAGG', 'CATGCTGGGCTGGCT')

# Output:
# AA

# print find_shortest_non_shared(example1, example2)

# Output:
# AAACC

print find_shortest_non_shared(example1, example2)

f.close()
