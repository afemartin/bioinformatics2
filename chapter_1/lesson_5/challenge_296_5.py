# -*- coding: utf-8 -*-


def suffix_trie_construction(text):

    start = 0

    graph = {'^': {}}

    while start < len(text):

        pattern = text[start:]

        current_node = graph['^']

        for c in pattern:

            if c in current_node:

                next_node = current_node[c]

            else:

                next_node = start if c == '$' else {}

                current_node[c] = next_node

            current_node = next_node

        start += 1

    return graph


def find_longest_repeat(text):

    # print suffix_trie_construction(text + '$')

    pattern = ''

    text_length = len(text)

    pattern_length = text_length - 1

    while pattern_length > 0:

        for start in range(0, text_length - pattern_length + 1):

            pattern = text[start:start+pattern_length]

            if start != text.find(pattern):

                return pattern

        pattern_length -= 1

    return pattern


f = open('dataset_296_5.txt', 'r')

example = f.read()

# print find_longest_repeat('ATAAATG')

# Output:
# AA

# print find_longest_repeat('ATATCGTTTTATCGTT')

# Output:
# TATCGTT

# print find_longest_repeat(example)

# Output:
# TTTCCATATACGGGACAAGGGTGAGCATTTCCGGGCTTGGATAGGGGCTGCAAGAAAATATCTGGACGTAAGAAG

print find_longest_repeat(example)

f.close()
