# -*- coding: utf-8 -*-


def find_pattern_childs(pattern, text):

    charset = ['A', 'T', 'G', 'C', '$']
    # charset = ['A', 'P', 'N', 'M', 'B', 'S', '$']

    childs = 0

    for c in charset:

        if pattern + c in text:

            childs += 1

    return childs


def suffix_trie_construction(text):

    start = 0

    edges = []

    while start < len(text):

        count = 1

        skip = 0

        while start + count <= len(text):

            pattern = text[start:start+count]

            childs = find_pattern_childs(pattern, text)

            if childs != 1:

                prefix = text[start:start+skip]

                found = pattern[skip:]

                edges.append((prefix, found))

                skip += len(found)

            count += 1

        start += 1

    # remove duplicates
    edges = list(set(edges))

    for edge in edges:

        print edge[1]


f = open('dataset_296_4.txt', 'r')

example = f.read()

# suffix_trie_construction('ATAAATG$')

# Output:
# AAATG$
# G$
# T
# ATG$
# TG$
# A
# A
# AAATG$
# G$
# T
# G$
# $

# suffix_trie_construction('PANAMABANANAS$')

suffix_trie_construction(example)

f.close()
